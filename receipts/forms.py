from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


class ReceiptForm(ModelForm):  # Step 1
    class Meta:  # Step 2
        model = Receipt  # Step 3
        fields = [  # Step 4
            "vendor",  # Step 4
            "total",  # Step 4
            "tax",
            "date",
            "category",
            "account",  # Step 4
        ]  # Step 4


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ["name"]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
